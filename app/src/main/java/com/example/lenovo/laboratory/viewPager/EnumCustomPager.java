package com.example.lenovo.laboratory.viewPager;

import com.example.lenovo.laboratory.R;

/**
 * Created by arimahardika on 22/02/2018.
 */

public enum EnumCustomPager {
    A(R.string.vp_one, R.layout.vp_layout_one),
    B(R.string.vp_two, R.layout.vp_layout_two),
    C(R.string.vp_three, R.layout.vp_layout_three),
    D(R.string.vp_four, R.layout.vp_layout_four),
    E(R.string.vp_five, R.layout.vp_layout_five),
    F(R.string.vp_six, R.layout.vp_layout_six);

    private int mTitleResId;
    private int mLayoutResId;

    EnumCustomPager(int mTitleResId, int mLayoutResId) {
        this.mTitleResId = mTitleResId;
        this.mLayoutResId = mLayoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }
}
