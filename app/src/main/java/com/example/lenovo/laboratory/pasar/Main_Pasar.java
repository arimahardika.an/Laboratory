package com.example.lenovo.laboratory.pasar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.laboratory.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arimahardika on 22/02/2018.
 */

public class Main_Pasar extends Activity {

    String DATA_SOURCE_URL = "https://sampankecil.scalingo.io/hargapasar";
    RecyclerView recyclerView;
    Adapter_Pasar adapter_pasar;
    List<Model_Pasar> list_pasar;

    ProgressDialog progressDialog;
    String loadingMessage = "Fetch Data";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pasar_main);

        recyclerView = findViewById(R.id.pasar_recylerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        list_pasar = new ArrayList<>();

        initializeDataPasar();
    }

    private void initializeDataPasar() {

        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };
        //picolo
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, DATA_SOURCE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Model_Pasar modelPasar = new Model_Pasar(
                                        jsonObject.getString("nama"),
                                        jsonObject.getString("jenis"),
                                        jsonObject.getString("satuan"),
                                        jsonObject.getString("harga_kemarin")
                                );
                                list_pasar.add(modelPasar);
                            }
                            adapter_pasar = new Adapter_Pasar(list_pasar, getApplicationContext());
                            recyclerView.setAdapter(adapter_pasar);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main_Pasar.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("VolleyError", error.toString());
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
