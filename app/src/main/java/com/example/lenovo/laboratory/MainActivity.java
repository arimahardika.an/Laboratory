package com.example.lenovo.laboratory;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.lenovo.laboratory.camera.Camera_main;
import com.example.lenovo.laboratory.customListView.CustomListView;
import com.example.lenovo.laboratory.displayFromPath.displayFromPath;
import com.example.lenovo.laboratory.expandableListView.Expand_Main;
import com.example.lenovo.laboratory.pasar.Main_Pasar;
import com.example.lenovo.laboratory.viewPager.MainViewPager;

public class MainActivity extends Activity {

    //<editor-fold desc="Button Initialize">
    Button customListView, cardView, scrollView,
            expandableListView, expandableJSON,
            viewPagerNoFragment, dataPasar, btnPopup,
            btnCamera, loadimagefromfilepath;

    Dialog popupDialog;
    //</editor-fold>

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        popupDialog = new Dialog(this);

        //<editor-fold desc="findViewById">
        customListView = findViewById(R.id.btn_clv);
        cardView = findViewById(R.id.btn_cv);
        scrollView = findViewById(R.id.btn_sv);
        expandableListView = findViewById(R.id.expandable);
        expandableJSON = findViewById(R.id.expandableJSON);
        viewPagerNoFragment = findViewById(R.id.viewPager);
        dataPasar = findViewById(R.id.dataPasar);
        btnPopup = findViewById(R.id.displayPopup);
        btnCamera = findViewById(R.id.camera);
        loadimagefromfilepath = findViewById(R.id.loadimagefromfilepath);

        //</editor-fold>

        loadimagefromfilepath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, displayFromPath.class);
                startActivity(i);
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, Camera_main.class);
                startActivity(i);
            }
        });

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.setContentView(R.layout.popup_window);
                popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                popupDialog.show();
            }
        });

        //<editor-fold desc="DONE">
        customListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, CustomListView.class);
                startActivity(i);
            }
        });

        scrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, ScrollView_Pariwisata.class);
                startActivity(i);
            }
        });

        expandableListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, Expand_Main.class);
                startActivity(i);
            }
        });

        expandableJSON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, com.example.lenovo.laboratory.expandableJsonCopiedFromTutorial.HeroMain.class);
                startActivity(i);
            }
        });

        viewPagerNoFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, MainViewPager.class);
                startActivity(i);
            }
        });

        dataPasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, Main_Pasar.class);
                startActivity(i);
            }
        });
        //</editor-fold>
    }
}
