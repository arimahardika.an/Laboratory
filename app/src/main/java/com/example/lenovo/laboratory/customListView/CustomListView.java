package com.example.lenovo.laboratory.customListView;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.lenovo.laboratory.R;

/**
 * Created by lenovo on 2/13/2018.
 */

public class CustomListView extends Activity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] titles = getResources().getStringArray(R.array.list_title);
        String[] desc = getResources().getStringArray(R.array.list_desc);
        int[] icons = getResources().getIntArray(R.array.list_icon);

        setContentView(R.layout.customlistview_mainpage);

        listView = findViewById(R.id.lv_mainPage);

        listView.setAdapter(new CustomListViewAdapter(this, titles, desc, icons));

    }
}
