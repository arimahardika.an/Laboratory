package com.example.lenovo.laboratory.expandableListView;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ExpandableListView;

import com.example.lenovo.laboratory.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by arimahardika on 19/02/2018.
 */

public class Expand_Main extends Activity {

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHashMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_main);

        ExpandableListView expandableListView = findViewById(R.id.elv_main);

        initializeData();

        Expand_Adapter expand_adapter = new Expand_Adapter(this, listDataHeader, listHashMap);

        expandableListView.setAdapter(expand_adapter);
    }

    private void initializeData() {
        listDataHeader = new ArrayList<>();
        listHashMap = new HashMap<>();

        String[] headerDataFromStringArrays = getResources().getStringArray(R.array.headerList);
        String[] childOneDataFromStringArrays = getResources().getStringArray(R.array.child_1);
        String[] childTwoDataFromStringArrays = getResources().getStringArray(R.array.child_2);
        String[] childThreeDataFromStringArrays = getResources().getStringArray(R.array.child_3);

        listDataHeader = new ArrayList<>(Arrays.asList(headerDataFromStringArrays));

        List<String> child1 = new ArrayList<>(Arrays.asList(childOneDataFromStringArrays));
        List<String> child2 = new ArrayList<>(Arrays.asList(childTwoDataFromStringArrays));
        List<String> child3 = new ArrayList<>(Arrays.asList(childThreeDataFromStringArrays));

        listHashMap = new HashMap<>();

        listHashMap.put(listDataHeader.get(0), child1);
        listHashMap.put(listDataHeader.get(1), child2);
        listHashMap.put(listDataHeader.get(2), child3);
    }
}
