package com.example.lenovo.laboratory.viewPager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by arimahardika on 22/02/2018.
 */

class AdapterCustomPager extends PagerAdapter {

    private Context mContext;

    public AdapterCustomPager(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        EnumCustomPager enumCustomPager = EnumCustomPager.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(enumCustomPager.getLayoutResId(),
                container, false);
        container.addView(viewGroup);
        return viewGroup;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return EnumCustomPager.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public CharSequence getPageTitle(int position) {
        EnumCustomPager enumCustomPager = EnumCustomPager.values()[position];
        return mContext.getString(enumCustomPager.getTitleResId());
    }
}
